package com.news.app.misc;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by shashika silva on 18/03/2017.
 */
@XmlRootElement
public class SourceResponse {
    private String status;
    private List<Source> sources;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }
}
