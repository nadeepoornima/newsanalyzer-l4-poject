package com.news.app.misc;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by shashika silva on 18/03/2017.
 */
@XmlRootElement
public class ArticleResponse {

    //@XmlElement
    private String status;

    //@XmlElement
    private String source;

    //@XmlElement
    private String sortBy;

    //@XmlElementWrapper
    //@XmlElement(name="articles")
    private List<Article> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
