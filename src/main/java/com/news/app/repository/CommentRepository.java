package com.news.app.repository;

import com.news.app.domain.Comment;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Comment entity.
 */
@SuppressWarnings("unused")
public interface CommentRepository extends JpaRepository<Comment,Long> {

    @Query("select u from Comment u where u.news.id = ?1")
    List<Comment> getCommentsByNews(Long id);
}
