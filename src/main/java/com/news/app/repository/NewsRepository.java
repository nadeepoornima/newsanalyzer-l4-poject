package com.news.app.repository;

import com.news.app.domain.Comment;
import com.news.app.domain.News;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the News entity.
 */
@SuppressWarnings("unused")
public interface NewsRepository extends JpaRepository<News,Long> {

    @Modifying
    @Transactional
    @Query("UPDATE News t set t.noOfViews = t.noOfViews + 1 WHERE t.id = :newsId")
    public void updateNoOfViews(@Param("newsId") Long newsId);

    @Query("SELECT n " +
        "  FROM Comment c , News n " +
        "  where c.news.id = n.id and n.tag1=:subcat and n.createBy.id =:aid and n.id!=:nid and " +
        "  ((n.tag2 IN (:tags)) or " +
        "  (n.tag3 IN (:tags)) or " +
        "  (n.tag4 IN (:tags)))" )
    //"  order by c.rating DESC")
    public List<News> getSimilarArticles(@Param("tags") Set<String> tags,
                                         @Param("subcat") String subcat,
                                         @Param("aid") Long aid, @Param("nid") Long nid, Pageable pageable);

    @Query("SELECT n.id,n.newsTitle " +
        "  FROM Comment c , News n " +
        "  where c.news.id = n.id and n.id!=:nid and " +
        "  ((n.tag2 IN (:tags)) or " +
        "  (n.tag3 IN (:tags)) or " +
        "  (n.tag4 IN (:tags))) group by n.id,n.newsTitle " )
    //"  order by c.rating DESC")
    public List<News> getConnectedArticles(@Param("tags") Set<String> tags,
                                         @Param("nid") Long nid, Pageable pageable);


    @Query("select n From News n" )
    //"  order by c.rating DESC")
    public List<News> allNews();

}
