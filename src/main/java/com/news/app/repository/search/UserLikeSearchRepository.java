package com.news.app.repository.search;

import com.news.app.domain.UserLike;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the UserLike entity.
 */
public interface UserLikeSearchRepository extends ElasticsearchRepository<UserLike, Long> {
}
