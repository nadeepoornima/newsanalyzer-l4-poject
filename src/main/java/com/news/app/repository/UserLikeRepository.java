package com.news.app.repository;

import com.news.app.domain.News;
import com.news.app.domain.User;
import com.news.app.domain.UserLike;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserLike entity.
 */
@SuppressWarnings("unused")
public interface UserLikeRepository extends JpaRepository<UserLike,Long> {

    public UserLike findByNewsAndCreateBy(News news, User createBy);
}
