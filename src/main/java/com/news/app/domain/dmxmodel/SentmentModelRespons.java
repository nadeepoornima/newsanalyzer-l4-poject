package com.news.app.domain.dmxmodel;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by shashika silva on 31/05/2017.
 */
@XmlRootElement
public class SentmentModelRespons {
    private String possitives ;

    private String negatives;

    public String getPossitives() {
        return possitives;
    }

    public void setPossitives(String possitives) {
        this.possitives = possitives;
    }

    public String getNegatives() {
        return negatives;
    }

    public void setNegatives(String negatives) {
        this.negatives = negatives;
    }
}
