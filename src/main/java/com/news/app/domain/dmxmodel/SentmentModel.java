package com.news.app.domain.dmxmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashika silva on 24/05/2017.
 */
@XmlRootElement
public class SentmentModel {

    private String[] possitives ;

    private String[] negatives;

    //@XmlElementWrapper( name="possitives" )
    public String[] getPossitives() {
        return possitives;
    }

    public void setPossitives(String[] possitives) {
        this.possitives = possitives;
    }

    //@XmlElementWrapper( name="negatives" )
    public String[] getNegatives() {
        return negatives;
    }

    public void setNegatives(String[] negatives) {
        this.negatives = negatives;
    }
}
