package com.news.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A News.
 */
@Entity
@Table(name = "news")
@Document(indexName = "news")
public class News implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "news_title", nullable = false)
    private String newsTitle;

    @NotNull
    @Column(name = "news_description", nullable = false)
    private String newsDescription;

    @Column(name = "no_of_likes")
    private Integer noOfLikes;

    @Column(name = "no_of_shares")
    private Integer noOfShares;

    @Column(name = "no_of_views")
    private Integer noOfViews;

    @Column(name = "emotions")
    private String emotions;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @ManyToOne
    private SubCategory subCategory;

    @OneToOne
    @JoinColumn(unique = true)
    private User createBy;

    @OneToOne
    @JoinColumn(unique = true)
    private User updateBy;

    @Column(name = "tag1")
    private String tag1;

    @Column(name = "tag2")
    private String tag2;

    @Column(name = "tag3")
    private String tag3;

    @Column(name = "tag4")
    private String tag4;

    @OneToMany(mappedBy = "news")
    @JsonIgnore
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(mappedBy = "news")
    @JsonIgnore
    private Set<UserLike> userLikes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public News newsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
        return this;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsDescription() {
        return newsDescription;
    }

    public News newsDescription(String newsDescription) {
        this.newsDescription = newsDescription;
        return this;
    }

    public void setNewsDescription(String newsDescription) {
        this.newsDescription = newsDescription;
    }

    public Integer getNoOfLikes() {
        return noOfLikes;
    }

    public News noOfLikes(Integer noOfLikes) {
        this.noOfLikes = noOfLikes;
        return this;
    }

    public void setNoOfLikes(Integer noOfLikes) {
        this.noOfLikes = noOfLikes;
    }

    public Integer getNoOfShares() {
        return noOfShares;
    }

    public News noOfShares(Integer noOfShares) {
        this.noOfShares = noOfShares;
        return this;
    }

    public void setNoOfShares(Integer noOfShares) {
        this.noOfShares = noOfShares;
    }

    public Integer getNoOfViews() {
        return noOfViews;
    }

    public News noOfViews(Integer noOfViews) {
        this.noOfViews = noOfViews;
        return this;
    }

    public void setNoOfViews(Integer noOfViews) {
        this.noOfViews = noOfViews;
    }

    public String getEmotions() {
        return emotions;
    }

    public News emotions(String emotions) {
        this.emotions = emotions;
        return this;
    }

    public void setEmotions(String emotions) {
        this.emotions = emotions;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public News isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public News createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public News updateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public News subCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
        return this;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public User getCreateBy() {
        return createBy;
    }

    public News createBy(User user) {
        this.createBy = user;
        return this;
    }

    public void setCreateBy(User user) {
        this.createBy = user;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public News updateBy(User user) {
        this.updateBy = user;
        return this;
    }

    public void setUpdateBy(User user) {
        this.updateBy = user;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public News comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public News addComments(Comment comment) {
        comments.add(comment);
        comment.setNews(this);
        return this;
    }

    public News removeComments(Comment comment) {
        comments.remove(comment);
        comment.setNews(null);
        return this;
    }

    public Set<UserLike> getUserLikes() {
        return userLikes;
    }

    public void setUserLikes(Set<UserLike> userLikes) {
        this.userLikes = userLikes;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public void setTag3(String tag3) {
        this.tag3 = tag3;
    }

    public String getTag4() {
        return tag4;
    }

    public void setTag4(String tag4) {
        this.tag4 = tag4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        News news = (News) o;
        if(news.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, news.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "News{" +
            "id=" + id +
            ", newsTitle='" + newsTitle + "'" +
            ", newsDescription='" + newsDescription + "'" +
            ", noOfLikes='" + noOfLikes + "'" +
            ", noOfShares='" + noOfShares + "'" +
            ", noOfViews='" + noOfViews + "'" +
            ", emotions='" + emotions + "'" +
            ", isActive='" + isActive + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            '}';
    }
}
