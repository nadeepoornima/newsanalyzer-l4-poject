package com.news.app.service.impl;

import com.news.app.domain.News;
import com.news.app.domain.User;
import com.news.app.service.UserLikeService;
import com.news.app.domain.UserLike;
import com.news.app.repository.UserLikeRepository;
import com.news.app.repository.search.UserLikeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserLike.
 */
@Service
@Transactional
public class UserLikeServiceImpl implements UserLikeService{

    private final Logger log = LoggerFactory.getLogger(UserLikeServiceImpl.class);

    @Inject
    private UserLikeRepository userLikeRepository;

    @Inject
    private UserLikeSearchRepository userLikeSearchRepository;

    /**
     * Save a userLike.
     *
     * @param userLike the entity to save
     * @return the persisted entity
     */
    public UserLike save(UserLike userLike) {
        log.debug("Request to save UserLike : {}", userLike);
        UserLike result = userLikeRepository.save(userLike);
        userLikeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the userLikes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserLike> findAll(Pageable pageable) {
        log.debug("Request to get all UserLikes");
        Page<UserLike> result = userLikeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one userLike by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserLike findOne(Long id) {
        log.debug("Request to get UserLike : {}", id);
        UserLike userLike = userLikeRepository.findOne(id);
        return userLike;
    }

    @Transactional(readOnly = true)
    public UserLike findByNewsAndCreateBy(News news, User createdBy) {
        UserLike userLike = userLikeRepository.findByNewsAndCreateBy(news,createdBy);
        return userLike;
    }

    /**
     *  Delete the  userLike by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserLike : {}", id);
        userLikeRepository.delete(id);
        userLikeSearchRepository.delete(id);
    }

    /**
     * Search for the userLike corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserLike> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserLikes for query {}", query);
        Page<UserLike> result = userLikeSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
