package com.news.app.service.impl;

import com.news.app.domain.User;
import com.news.app.repository.UserRepository;
import com.news.app.security.SecurityUtils;
import com.news.app.service.NewsService;
import com.news.app.domain.News;
import com.news.app.repository.NewsRepository;
import com.news.app.repository.search.NewsSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing News.
 */
@Service
@Transactional
public class NewsServiceImpl implements NewsService{

    private final Logger log = LoggerFactory.getLogger(NewsServiceImpl.class);

    @Inject
    private NewsRepository newsRepository;

    @Inject
    private NewsSearchRepository newsSearchRepository;

    @Inject
    private UserRepository userRepository;

    /**
     * Save a news.
     *
     * @param news the entity to save
     * @return the persisted entity
     */
    public News save(News news) {
        log.debug("Request to save News : {}", news);
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        if(news.getId() == null){
            news.setCreateDate(ZonedDateTime.now(ZoneId.systemDefault()));
            news.setUpdateDate(ZonedDateTime.now(ZoneId.systemDefault()));
            news.setNoOfViews(0);
            news.setNoOfLikes(0);
            news.setNoOfShares(0);
            news.setCreateBy(user);
            news.setUpdateBy(user);
        }else{
            news.setUpdateDate(ZonedDateTime.now(ZoneId.systemDefault()));
            news.setUpdateBy(user);
        }
        News result = newsRepository.save(news);
        newsSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the news.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<News> findAll(Pageable pageable) {
        log.debug("Request to get all News");
        Page<News> result = newsRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public List<News> findAllWithoutPAgination() {
        log.debug("Request to get all News");
        return newsRepository.findAll();
    }

    /**
     *  Get one news by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public News findOne(Long id) {
        log.debug("Request to get News : {}", id);
        News news = newsRepository.findOne(id);
        return news;
    }

    /**
     *  Delete the  news by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete News : {}", id);
        newsRepository.delete(id);
        newsSearchRepository.delete(id);
    }

    /**
     * Search for the news corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<News> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of News for query {}", query);
        Page<News> result = newsSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public void updateNoOfViews(Long id){
        newsRepository.updateNoOfViews(id);
    }

    @Transactional(readOnly = true)
    public List<News> findSimilarArticles(Set<String> tags, String sub, Long auther, Long nid) {
        log.debug("Request to get all News");
        Pageable pageable = new PageRequest(0,10);
        List<News> newss;
        if(auther == null && sub == null){
            newss = newsRepository.getConnectedArticles(tags,nid,pageable);
        }else{
            newss = newsRepository.getSimilarArticles(tags,sub,auther,nid,pageable);
        }
        return newss;
    }


    @Transactional(readOnly = true)
    public List<News> allNews() {
        log.debug("Request to get all News");
        List<News> newss = newsRepository.allNews();
        return newss;
    }
}
