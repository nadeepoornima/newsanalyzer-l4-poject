package com.news.app.service;

import com.news.app.domain.News;
import com.news.app.domain.User;
import com.news.app.domain.UserLike;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing UserLike.
 */
public interface UserLikeService {

    /**
     * Save a userLike.
     *
     * @param userLike the entity to save
     * @return the persisted entity
     */
    UserLike save(UserLike userLike);

    /**
     *  Get all the userLikes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UserLike> findAll(Pageable pageable);

    /**
     *  Get the "id" userLike.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UserLike findOne(Long id);

    /**
     *  Delete the "id" userLike.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the userLike corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UserLike> search(String query, Pageable pageable);

    UserLike findByNewsAndCreateBy(News news, User createdBy);
}
