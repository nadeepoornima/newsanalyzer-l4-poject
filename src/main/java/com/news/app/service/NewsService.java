package com.news.app.service;

import com.news.app.domain.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * Service Interface for managing News.
 */
public interface NewsService {

    /**
     * Save a news.
     *
     * @param news the entity to save
     * @return the persisted entity
     */
    News save(News news);

    /**
     *  Get all the news.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<News> findAll(Pageable pageable);

    /**
     *  Get the "id" news.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    News findOne(Long id);

    /**
     *  Delete the "id" news.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the news corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<News> search(String query, Pageable pageable);

    void updateNoOfViews(Long id);

    List<News> findAllWithoutPAgination();

    List<News> findSimilarArticles(Set<String> tags, String sub, Long auther, Long nid);

    List<News> allNews();
}
