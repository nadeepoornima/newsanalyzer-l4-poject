package com.news.app.service;

import com.news.app.domain.SubCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing SubCategory.
 */
public interface SubCategoryService {

    /**
     * Save a subCategory.
     *
     * @param subCategory the entity to save
     * @return the persisted entity
     */
    SubCategory save(SubCategory subCategory);

    /**
     *  Get all the subCategories.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SubCategory> findAll(Pageable pageable);

    /**
     *  Get the "id" subCategory.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SubCategory findOne(Long id);

    /**
     *  Delete the "id" subCategory.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the subCategory corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SubCategory> search(String query, Pageable pageable);
}
