package com.news.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.news.app.domain.UserLike;
import com.news.app.service.UserLikeService;
import com.news.app.web.rest.util.HeaderUtil;
import com.news.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserLike.
 */
@RestController
@RequestMapping("/api")
public class UserLikeResource {

    private final Logger log = LoggerFactory.getLogger(UserLikeResource.class);
        
    @Inject
    private UserLikeService userLikeService;

    /**
     * POST  /user-likes : Create a new userLike.
     *
     * @param userLike the userLike to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userLike, or with status 400 (Bad Request) if the userLike has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-likes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserLike> createUserLike(@Valid @RequestBody UserLike userLike) throws URISyntaxException {
        log.debug("REST request to save UserLike : {}", userLike);
        if (userLike.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userLike", "idexists", "A new userLike cannot already have an ID")).body(null);
        }
        UserLike result = userLikeService.save(userLike);
        return ResponseEntity.created(new URI("/api/user-likes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userLike", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-likes : Updates an existing userLike.
     *
     * @param userLike the userLike to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userLike,
     * or with status 400 (Bad Request) if the userLike is not valid,
     * or with status 500 (Internal Server Error) if the userLike couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-likes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserLike> updateUserLike(@Valid @RequestBody UserLike userLike) throws URISyntaxException {
        log.debug("REST request to update UserLike : {}", userLike);
        if (userLike.getId() == null) {
            return createUserLike(userLike);
        }
        UserLike result = userLikeService.save(userLike);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userLike", userLike.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-likes : get all the userLikes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userLikes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/user-likes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserLike>> getAllUserLikes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of UserLikes");
        Page<UserLike> page = userLikeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-likes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-likes/:id : get the "id" userLike.
     *
     * @param id the id of the userLike to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userLike, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/user-likes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserLike> getUserLike(@PathVariable Long id) {
        log.debug("REST request to get UserLike : {}", id);
        UserLike userLike = userLikeService.findOne(id);
        return Optional.ofNullable(userLike)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /user-likes/:id : delete the "id" userLike.
     *
     * @param id the id of the userLike to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/user-likes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserLike(@PathVariable Long id) {
        log.debug("REST request to delete UserLike : {}", id);
        userLikeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userLike", id.toString())).build();
    }

    /**
     * SEARCH  /_search/user-likes?query=:query : search for the userLike corresponding
     * to the query.
     *
     * @param query the query of the userLike search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/user-likes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserLike>> searchUserLikes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of UserLikes for query {}", query);
        Page<UserLike> page = userLikeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/user-likes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
