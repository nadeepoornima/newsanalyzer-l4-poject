package com.news.app.web.rest;

import com.news.app.domain.News;
import com.news.app.domain.dmxmodel.NewsRequest;
import com.news.app.misc.SourceResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by shashika silva on 14/05/2017.
 */
public class RestClient {
    public static void main(String[] args) {
        try {

            Client client = Client.create();
            String url = "http://localhost:54540/api/Contact?likes=5&shares=5&views=6&author=5";
            WebResource webResource = client
                .resource(url);
            ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }
            NewsRequest output = response.getEntity(NewsRequest.class);

            System.out.println("Output from Server .... \n");
            System.out.println(output);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
