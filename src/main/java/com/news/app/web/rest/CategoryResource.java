package com.news.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.news.app.domain.Category;
import com.news.app.domain.Comment;
import com.news.app.domain.News;
import com.news.app.domain.SubCategory;
import com.news.app.misc.*;
import com.news.app.service.*;
import com.news.app.web.rest.util.HeaderUtil;
import com.news.app.web.rest.util.PaginationUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Category.
 */
@RestController
@RequestMapping("/api")
public class CategoryResource {

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    @Inject
    private CategoryService categoryService;
    @Inject
    private NewsService newsService;
    @Inject
    private UserService userService;
    @Inject
    private CommentService commentService;
    @Inject
    private SubCategoryService subCategoryService;


    /**
     * POST  /categories : Create a new category.
     *
     * @param category the category to create
     * @return the ResponseEntity with status 201 (Created) and with body the new category, or with status 400 (Bad Request) if the category has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/categories",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Category> createCategory(@Valid @RequestBody Category category) throws URISyntaxException {
        log.debug("REST request to save Category : {}", category);
        if (category.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("category", "idexists", "A new category cannot already have an ID")).body(null);
        }
        Category result = categoryService.save(category);
        return ResponseEntity.created(new URI("/api/categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("category", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /categories : Updates an existing category.
     *
     * @param category the category to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated category,
     * or with status 400 (Bad Request) if the category is not valid,
     * or with status 500 (Internal Server Error) if the category couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/categories",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Category> updateCategory(@Valid @RequestBody Category category) throws URISyntaxException {
        log.debug("REST request to update Category : {}", category);
        if (category.getId() == null) {
            return createCategory(category);
        }
        Category result = categoryService.save(category);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("category", category.getId().toString()))
            .body(result);
    }

    /**
     * GET  /categories : get all the categories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of categories in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/categories",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Category>> getAllCategories(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Categories");
        Page<Category> page = categoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /categories/:id : get the "id" category.
     *
     * @param id the id of the category to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the category, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/categories/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Category> getCategory(@PathVariable Long id) {
        log.debug("REST request to get Category : {}", id);
        Category category = categoryService.findOne(id);
        return Optional.ofNullable(category)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /categories/:id : delete the "id" category.
     *
     * @param id the id of the category to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/categories/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.debug("REST request to delete Category : {}", id);
        categoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("category", id.toString())).build();
    }

    /**
     * SEARCH  /_search/categories?query=:query : search for the category corresponding
     * to the query.
     *
     * @param query the query of the category search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/categories",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Category>> searchCategories(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Categories for query {}", query);
        Page<Category> page = categoryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/populate-cat",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void populate()
        throws URISyntaxException {

        List<Source> sources = getSources();
        Set<String> cats = new HashSet<>();
        if(sources != null && sources.size() > 0){
            for (Source source : sources){
                cats.add(source.getCategory());
            }
        }

        for (String cat :cats){
/*            Category category = new Category();
            category.setName(cat);
            category.setId(getCatIDByname(cat));*/
            //categoryService.save(category);
            SubCategory subCategory = new SubCategory();
            subCategory.setName(cat);
            subCategory.setCategory(categoryService.findOne(getCatIDByname(cat)));
            //subCategoryService.save(subCategory);
            System.out.println(cat);
        }

    }

    @RequestMapping(value = "/populate-news",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void populateNews()
        throws URISyntaxException {

        try {

            List<Source> sources = getSources();
            int no =0;
            if(sources != null && sources.size() > 0){
                for (Source source : sources){

                    List<Article> articles =  getArticles(source.getId());
                    if(articles != null && articles.size() > 0 ){
                        for(Article article : articles){
                            //System.out.println("article name : "+ article.getTitle() + "\n");

                            News news = new News();
                            news.setNewsDescription(article.getDescription());
                            news.setNewsTitle(article.getTitle());

                            if(subCategoryService.findOne(getSubCatIDByname(source.getCategory())) == null){
                                System.out.println("---- "+source.getCategory());

                            }
                            news.setSubCategory(subCategoryService.findOne(getSubCatIDByname(source.getCategory())));
                            newsService.save(news);

                            //newsService.save(news);
                            no++;
                        }
                    }
                }
            }

            System.out.println("count  "+ no);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<Source> getSources(){
        Client client = Client.create();
        String url = "http://newsapi.org/v1/sources?apiKey=335879a98b1848849a3204b6ab4c1248";
        WebResource webResource = client
            .resource(url);
        ClientResponse response = webResource.accept("application/json")
            .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                + response.getStatus());
        }
        SourceResponse output = response.getEntity(SourceResponse.class);
        //System.out.println("Server response sources .... \n");
        //System.out.println("sources size "+output.getSources().size());
        return output.getSources();
    }

    public List<Article> getArticles(String source){
        Client client = Client.create();
        String url = "http://newsapi.org/v1/articles?source="+source+"&apiKey=335879a98b1848849a3204b6ab4c1248";
        WebResource webResource = client
            .resource(url);
        ClientResponse response = webResource.accept("application/json")
            .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed article - "+ source + " : HTTP error code : "
                + response.getStatus());
        }
        ArticleResponse output = response.getEntity(ArticleResponse.class);
        //System.out.println("Server response articles .... \n");
        //System.out.println(output);
        return output.getArticles();
    }

    public Long getCatIDByname(String cat){

        if(cat.equals("buisiness")){
            return 20l;
        }else if(cat.equals("sport")){
            return 21l;
        }else if(cat.equals("general")){
            return 24l;
        }else if(cat.equals("politics")){
            return 25l;
        }else if(cat.equals("gaming")){
            return 26l;
        }else if(cat.equals("music")){
            return 27l;
        }else if(cat.equals("entertainment")){
            return 29l;
        }else if(cat.equals("science-and-nature")){
            return 30l;
        }else if(cat.equals("technology")){
            return 31l;
        }
        return 24l;
    }

    public Long getSubCatIDByname(String cat){

        if(cat.equals("business")){
            return 20l;
        }else if(cat.equals("sport")){
            return 24l;
        }else if(cat.equals("general")){
            return 16l;
        }else if(cat.equals("politics")){
            return 17l;
        }else if(cat.equals("gaming")){
            return 18l;
        }else if(cat.equals("music")){
            return 19l;
        }else if(cat.equals("entertainment")){
            return 21l;
        }else if(cat.equals("science-and-nature")){
            return 22l;
        }else if(cat.equals("technology")){
            return 23l;
        }
        return 26l;
    }

    @RequestMapping(value = "/populate-comments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void populateComments()
        throws URISyntaxException {
        List<News> newsList = newsService.findAllWithoutPAgination();
        for (News news : newsList){
            for(int i=0;i<2;i++){
                int randomNum = ThreadLocalRandom.current().nextInt(0, 3);
                Comment comment = new Comment();
                comment.setUserComment(CommentGen.negativeComment());
                comment.setIsActive(true);
                comment.setNews(news);
                comment.setRating(randomNum);
                comment.setCreateBy(userService.findOne(Long.valueOf(randomNum)));
                System.out.println(randomNum+ "-->"+ comment.getUserComment());
                commentService.save(comment);
            }
        }
    }
    @RequestMapping(value = "/populate-tags",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void populateTags()
        throws URISyntaxException {
        List<News> newsList = newsService.findAllWithoutPAgination();

        for (News news : newsList){

            news.setTag1(news.getSubCategory().getName());
            int likes = ThreadLocalRandom.current().nextInt(0, 5);
            int sahres = ThreadLocalRandom.current().nextInt(0, 200);
            int views = ThreadLocalRandom.current().nextInt(0, 500);
            news.setNoOfLikes(likes);
            news.setNoOfShares(sahres);
            news.setNoOfViews(views);
            List<String> tags = OpenNTest.getTags(news.getNewsDescription());
            if(tags.size() > 0){
                news.setTag2(tags.get(0));
            }
            if(tags.size() > 1){
                news.setTag3(tags.get(1));
            }
            if(tags.size() > 2){
                news.setTag3(tags.get(2));
            }
            if(tags.size() > 3){
                news.setTag4(tags.get(3));
            }
            newsService.save(news);

        }
    }


}
