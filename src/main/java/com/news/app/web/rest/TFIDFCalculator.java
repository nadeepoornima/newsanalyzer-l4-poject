package com.news.app.web.rest;

import com.news.app.web.WeightModel;

import java.util.*;

public class TFIDFCalculator {
    /**
     * @param doc  list of strings
     * @param term String represents a term
     * @return term frequency of term in document
     */
    public double tf(List<String> doc, String term) {
        double result = 0;
        for (String word : doc) {
            if (term.equalsIgnoreCase(word))
                result++;
        }
        return result / doc.size();
    }

    /**
     * @param docs list of list of strings represents the dataset
     * @param term String represents a term
     * @return the inverse term frequency of term in documents
     */
    public double idf(List<List<String>> docs, String term) {
        double n = 0;
        for (List<String> doc : docs) {
            for (String word : doc) {
                if (term.equalsIgnoreCase(word)) {
                    n++;
                    break;
                }
            }
        }
        return Math.log(docs.size() / n);
    }

    /**
     * @param doc  a text document
     * @param docs all documents
     * @param term term
     * @return the TF-IDF of term
     */
    public double tfIdf(List<String> doc, List<List<String>> docs, String term) {
        return tf(doc, term) * idf(docs, term);

    }

    public static void main(String[] args) {



        List<String> doc1 = Arrays.asList("Lorem", "ipsum", "dolor", "ipsum", "sit", "ipsum");
        List<String> doc2 = Arrays.asList("Vituperata", "incorrupte", "at", "ipsum", "pro", "quo");
        List<String> doc3 = Arrays.asList("Has", "persius", "disputationi", "id", "simul");
        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        /*TFIDFCalculator calculator = new TFIDFCalculator();
        double tfidf = calculator.tfIdf(doc1, documents, "the");
        System.out.println("TF-IDF (ipsum) = " + tfidf);*/

        TFIDFCalculator tfidfCalculator = new TFIDFCalculator();
        tfidfCalculator.getSummeryWithWeight(documents);

    }

    public Set<String> getSummeryWithWeight(List<List<String>> documents){

        List<WeightModel> weightModels = new ArrayList<>();
        TFIDFCalculator calculator = new TFIDFCalculator();

        for (List<String> docs: documents){
            Integer i =0;
            for(String doc : docs){
                i++;
                double tfidf = calculator.tfIdf(docs, documents, doc);
                WeightModel weightModel = new WeightModel();
                weightModel.setModel(i.toString());
                weightModel.setmWord(doc);
                weightModel.setWeight(tfidf);
                weightModels.add(weightModel);
            }
        }

        Collections.sort(weightModels, new Comparator<WeightModel>() {
            @Override
            public int compare(WeightModel c1, WeightModel c2) {
                return Double.compare(c1.getWeight(), c2.getWeight());
            }
        });
        Set<String> strings = new HashSet<String>();
        for(WeightModel weightModel : weightModels){
            strings.add(weightModel.getmWord());
        }
        /*String finals = "";
        int finalCount =0;
        for(String model : strings){
            finalCount ++;
            if(finalCount < 20){
                finals = finals + "," + model;
            }else{
                continue;
            }
        }*/
        return strings;
    }


}
