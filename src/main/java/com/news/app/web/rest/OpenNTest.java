package com.news.app.web.rest;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.tokenize.DetokenizationDictionary;
import opennlp.tools.tokenize.DictionaryDetokenizer;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;
import org.apache.commons.lang3.ArrayUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

//extract noun phrases from a single sentence using OpenNLP
public class OpenNTest {

    //static String sentence = "US President Donald Trump and Prime Minister Malcolm Turnbull meet in New York City after their meeting was delayed by several hours.";

    static Set<String> nounPhrases;

    public static List<String> getTags(String sentence) {
        nounPhrases = new HashSet<>();
        List<String> tags = new ArrayList<>();
        InputStream modelInParse = null;
        List<String> avoidList = new ArrayList<>();
        avoidList.add("the");
        avoidList.add("a");
        avoidList.add("an");
        avoidList.add("his");


        try {
            //load chunking model
            modelInParse = new FileInputStream("en-parser-chunking.bin"); //from http://opennlp.sourceforge.net/models-1.5/
            ParserModel model = new ParserModel(modelInParse);

            //create parse tree
            Parser parser = ParserFactory.create(model);
            Tokenizer tokenizer = new SimpleTokenizer();
            tokenizer.tokenize("US,President,Donald Trump,Prime Minister,Malcolm Turnbull,meet,in,New York City");
            Parse topParses[] = ParserTool.parseLine(sentence, parser, tokenizer,1);

            //call subroutine to extract noun phrases
            for (Parse p : topParses)
                getNounPhrases(p);

            //print noun phrases
            for (String s : nounPhrases) {
                String[] arr = s.split(" ");

                for(String a : arr) {

                    if(tags.size() == 4 ){
                        return tags;
                    }

                    if (!avoidList.contains(a)) {

                        if(a.equals("the")){
                            System.out.println();
                        }

                        tags.add(a.toLowerCase());
                    }
                }
            }

            //The Call
            //the Wild?
            //The Call of the Wild? //punctuation remains on the end of sentence
            //the author of The Call of the Wild?
            //the author
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (modelInParse != null) {
                try {
                    modelInParse.close();
                } catch (IOException e) {
                }
            }
        }
        return tags;
    }

    //recursively loop through tree, extracting noun phrases
    public static void getNounPhrases(Parse p) {

        if (p.getType().equals("NP")) { //NP=noun phrase
            nounPhrases.add(p.getCoveredText());
        }
        for (Parse child : p.getChildren())
            getNounPhrases(child);
    }

    public static void main(String[] args) {

    /*OpenNTest openNTest = new OpenNTest();
        //OpenNTest.getTagsSet("US,President,Donald Trump,Prime Minister,Malcolm Turnbull,meet,in,New York City");
        OpenNTest.getTagsSet("US President Donald Trump and Prime Minister Malcolm Turnbull meet in New York City after their meeting was delayed by several hours.");

        for(String a : nounPhrases){
            System.out.println(a);
        }*/

        String tokens[] = {"We", "are", "living", "in", "an", "Environment",
            ",", "where", "multiple", "Hardware", "Architectures", "and",
            "Multiple", "platforms", "presents", ".", "So", "it", "is",
            "very", "difficult", "to", "write", ",", "compile", "and",
            "link", "the", "same", "Application", ",", "for", "each",
            "platform", "and", "each", "Architecture", "separately", ".",
            "The", "Java", "Programming", "Language", "solves", "all",
            "the", "above", "problems", ".", "The", "Java", "programming",
            "language", "platform", "provides", "a", "portable", ",",
            "interpreted", ",", "high-performance", ",", "simple", ",",
            "object-oriented", "programming", "language", "and",
            "supporting", "run-time", "environment", "."};

        String data = deTokenize(tokens,
            DetokenizationDictionary.Operation.MOVE_LEFT);
        System.out.println(data);
    }

    public static Set<String> getTagsSet(String sentence) {
        nounPhrases = new HashSet<>();
        List<String> tags = new ArrayList<>();
        InputStream modelInParse = null;
        List<String> avoidList = new ArrayList<>();
        avoidList.add("the");
        avoidList.add("a");
        avoidList.add("an");
        avoidList.add("his");


        try {
            //load chunking model
            modelInParse = new FileInputStream("en-parser-chunking.bin"); //from http://opennlp.sourceforge.net/models-1.5/
            ParserModel model = new ParserModel(modelInParse);

            //create parse tree
            Parser parser = ParserFactory.create(model);
            Parse topParses[] = ParserTool.parseLine(sentence, parser, 1);

            //call subroutine to extract noun phrases
            for (Parse p : topParses)
                getNounPhrases(p);
            //The Call
            //the Wild?
            //The Call of the Wild? //punctuation remains on the end of sentence
            //the author of The Call of the Wild?
            //the author
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (modelInParse != null) {
                try {
                    modelInParse.close();
                } catch (IOException e) {
                }
            }
        }
        return nounPhrases;
    }

    public static String deTokenize(String[] tokens,
                                    DetokenizationDictionary.Operation operation) {
        DetokenizationDictionary.Operation[] operations = new DetokenizationDictionary.Operation[tokens.length];

        for (int i = 0; i < tokens.length; i++) {
            operations[i] = operation;
        }

        DetokenizationDictionary dictionary = new DetokenizationDictionary(
            tokens, operations);
        DictionaryDetokenizer detokenizer = new DictionaryDetokenizer(
            dictionary);

        return detokenizer.detokenize(tokens, " ");
    }
}
