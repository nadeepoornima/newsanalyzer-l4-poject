package com.news.app.web.rest;

import java.util.Random;

/**
 * Created by shashika silva on 06/05/2017.
 */
public class CommentGen {

    public static String possitiveComment() {
        String[] article = new String[6];
        String[] adj = new String[5];
        String[] noun = new String[8];
        String[] verb = new String[3];

        article[0] = "Cool";
        article[1] = "Great,";
        article[2] = "";
        article[3] = "That's";
        article[4] = "Wooow,";
        article[5] = "Lovely ,";

        adj[0] = "good";
        adj[1] = "pleasant";
        adj[2] = "great";
        adj[3] = "super";
        adj[4] = "superb";

        noun[0] = "thing";
        noun[1] = "news";
        noun[2] = "article";
        noun[3] = "post";
        noun[4] = "one";
        noun[5] = "information";
        noun[6] = "info";
        noun[7] = "detailed info";

        verb[0] = "";
        verb[1] = "";
        verb[2] = "";

        // Test for randomSentence
        //for (int i=0 ; i<5;i++)
        //System.out.println(randomSentence(article, adj, noun, verb));
        return randomSentence(article, adj, noun, verb);
    }

    public static String negativeComment() {
        String[] article = new String[6];
        String[] adj = new String[3];
        String[] noun = new String[8];
        String[] verb = new String[3];

        article[0] = "Not Cool,";
        article[1] = "Unlike,";
        article[2] = "";
        article[3] = "That's";
        article[4] = "Bad,";
        article[5] = "Worst,";

        adj[0] = "not good";
        adj[1] = "not pleasant";
        adj[2] = "not great";

        noun[0] = "thing";
        noun[1] = "news";
        noun[2] = "article";
        noun[3] = "post";
        noun[4] = "one";
        noun[5] = "information";
        noun[6] = "info";
        noun[7] = "detailed info";

        verb[0] = "";
        verb[1] = "";
        verb[2] = "";

        // Test for randomSentence
        //for (int i=0 ; i<5;i++)
        //System.out.println(randomSentence(article, adj, noun, verb));
        return randomSentence(article, adj, noun, verb);
    }

    public static String randomSentence(String[] article, String[] adj, String[] noun, String[] verb) {

        Random rand = new Random();

        int articledx = rand.nextInt(article.length);
        int adjdx = rand.nextInt(adj.length);
        int noundx = rand.nextInt(noun.length);
        int verbdx = rand.nextInt(verb.length);

        String sentence = article[articledx] + " " + adj[adjdx] + " " + noun[noundx] + " " + verb[verbdx] + ".";

        return sentence.trim();
    }
}
