package com.news.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.news.app.domain.News;
import com.news.app.domain.User;
import com.news.app.domain.UserLike;
import com.news.app.domain.dmxmodel.NewsRequest;
import com.news.app.domain.dmxmodel.SentmentModel;
import com.news.app.domain.dmxmodel.SentmentModelRespons;
import com.news.app.repository.UserRepository;
import com.news.app.security.SecurityUtils;
import com.news.app.service.NewsService;
import com.news.app.service.UserLikeService;
import com.news.app.web.rest.util.HeaderUtil;
import com.news.app.web.rest.util.PaginationUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import opennlp.tools.tokenize.DetokenizationDictionary;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST controller for managing News.
 */
@RestController
@RequestMapping("/api")
public class NewsResource {

    private final Logger log = LoggerFactory.getLogger(NewsResource.class);

    @Inject
    private NewsService newsService;

    @Inject
    private UserLikeService userLikeService;

    @Inject
    private UserRepository userRepository;

    private String jhippoProxy = "http://www.jwordhip/";



    /**
     * POST  /news : Create a new news.
     *
     * @param news the news to create
     * @return the ResponseEntity with status 201 (Created) and with body the new news, or with status 400 (Bad Request) if the news has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/news",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<News> createNews(@Valid @RequestBody News news) throws URISyntaxException {
        log.debug("REST request to save News : {}", news);
        if (news.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("news", "idexists", "A new news cannot already have an ID")).body(null);
        }
        News result = newsService.save(news);
        return ResponseEntity.created(new URI("/api/news/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("news", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news : Updates an existing news.
     *
     * @param news the news to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated news,
     * or with status 400 (Bad Request) if the news is not valid,
     * or with status 500 (Internal Server Error) if the news couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/news",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<News> updateNews(@Valid @RequestBody News news) throws URISyntaxException {
        log.debug("REST request to update News : {}", news);
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        if (news.getId() == null) {
            news.setCreateDate(ZonedDateTime.now(ZoneId.systemDefault()));
            news.setUpdateDate(ZonedDateTime.now(ZoneId.systemDefault()));
            news.setCreateBy(user);
            news.setUpdateBy(user);
            return createNews(news);
        }else{
            News old = newsService.findOne(news.getId());
            if(old != null &&
               news.getNoOfLikes() != old.getNoOfLikes() &&
               (userLikeService.findByNewsAndCreateBy(news,user) == null)){
                UserLike userLike = new UserLike();
                userLike.setCreateBy(user);
                userLike.isActive(true);
                userLike.setIsLike(true);
                userLike.updateBy(user);
                userLike.setNews(news);
                userLikeService.save(userLike);
            }else {
                news.setNoOfLikes(old.getNoOfLikes());
            }

        }
        news.setUpdateBy(user);
        news.setUpdateDate(ZonedDateTime.now(ZoneId.systemDefault()));
        News result = newsService.save(news);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("news", news.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news : get all the news.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of news in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/news",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<News>> getAllNews(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of News");
        Page<News> page = newsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news/:id : get the "id" news.
     *
     * @param id the id of the news to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the news, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/news/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<News> getNews(@PathVariable Long id) {
        log.debug("REST request to get News : {}", id);
        News news = newsService.findOne(id);
        newsService.updateNoOfViews(id);
        return Optional.ofNullable(news)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /news/:id : delete the "id" news.
     *
     * @param id the id of the news to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/news/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNews(@PathVariable Long id) {
        log.debug("REST request to delete News : {}", id);
        newsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("news", id.toString())).build();
    }

    /**
     * SEARCH  /_search/news?query=:query : search for the news corresponding
     * to the query.
     *
     * @param query the query of the news search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/news",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<News>> searchNews(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of News for query {}", query);
        Page<News> page = newsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/similarArticles",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<News>> similarNews(@QueryParam("aid") String aid,@QueryParam("nid") Long nid)
        throws URISyntaxException {
        NewsRequest output = new NewsRequest();
        News oldOne = newsService.findOne(nid);
        List<News> newss = new ArrayList<>();
        List<News> finalList = new ArrayList<>();
        log.debug("REST request to get similar articles");
        try {
            Client client = Client.create();
            String url = "http://localhost:54540/api/Contact?likes="+oldOne.getNoOfLikes().toString()+"&shares="+oldOne.getNoOfShares().toString()+"&views="+oldOne.getNoOfViews().toString()+"&author="+aid;
            WebResource webResource = client
                .resource(url);
            ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }
            output = response.getEntity(NewsRequest.class);

            Set<String> tags = new HashSet<>();
            if(StringUtils.isNotEmpty(output.getTag1())){
                tags.add(output.getTag1());
            }else{
                tags.add("general");
                output.setTag1("general");
            }

            if(StringUtils.isNotEmpty(output.getTag2())){
                tags.add(output.getTag2());
            }

            if(StringUtils.isNotEmpty(output.getTag3())){
                tags.add(output.getTag3());
            }

            if(StringUtils.isNotEmpty(output.getTag4())){
                tags.add(output.getTag4());
            }

            newss =  newsService.findSimilarArticles(tags,output.getTag1(),Long.parseLong(aid),nid);
            Set<Long> uniqids = new HashSet<>();


            TFIDFCalculator tfidfCalculator = new TFIDFCalculator();
            List<List<String>> documents = new ArrayList<>();
            String relatedSummery= "";
            String curSumry= "";

            for(int k=0;k<newss.size();k++){
                documents = new ArrayList<>();
                if(uniqids.contains(newss.get(k).getId())){
                    continue;
                }else{
                    uniqids.add(newss.get(k).getId());
                    finalList.add(newss.get(k));
                }

                  News n = newsService.findOne(newss.get(k).getId());
                    List<String> items = Arrays.asList(n.getNewsDescription().split(" "));
                    documents.add(items);

                Set<String> strings = tfidfCalculator.getSummeryWithWeight(documents);
                System.out.println("------------------- "+strings+" ------------------");
                relatedSummery =relatedSummery + OpenNTest.deTokenize(strings.stream().toArray(ne -> new String[ne]), DetokenizationDictionary.Operation.MOVE_LEFT);

            }


            System.out.println(relatedSummery);

            //relatedSummery = relatedSummery.replace("<html>","");
            //relatedSummery = relatedSummery.replace("</html>","");

            finalList.get(0).setEmotions(relatedSummery);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(finalList, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/connectedArticles",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<News>> connectedArticles(@QueryParam("nid") Long nid)
        throws URISyntaxException {
        News old = newsService.findOne(nid);
        NewsRequest output = new NewsRequest();
        List<News> newss = new ArrayList<>();
        log.debug("REST request to get similar articles");
        try {
            Client client = Client.create();
            String url = "http://localhost:54540/api/ConnectedArticles?nids="+nid;
            WebResource webResource = client
                .resource(url);
            ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }
            output = response.getEntity(NewsRequest.class);
            News news = new News();
            news.setId(output.getId());
            news.setNewsTitle(output.getNewsTitle());
            newss.add(news);


        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(newss, headers, HttpStatus.OK);
    }


    @RequestMapping(value = "/publishArea",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Set<String>> publishArea(){
        NewsRequest output = new NewsRequest();
        Set<String> tags = new HashSet<>();
        log.debug("REST request to get similar articles");
        try {
            Client client = Client.create();
            String url = "http://localhost:54540/api/PublishArea?likes=5&shares=5&views=6";
            WebResource webResource = client
                .resource(url);
            ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }
            output = response.getEntity(NewsRequest.class);


            if(StringUtils.isNotEmpty(output.getTag1())){
                tags.add(output.getTag1());
            }else{
                tags.add("general");
                output.setTag1("general");
            }

            if(StringUtils.isNotEmpty(output.getTag2())){
                tags.add(output.getTag2());
            }

            if(StringUtils.isNotEmpty(output.getTag3())){
                tags.add(output.getTag3());
            }

            if(StringUtils.isNotEmpty(output.getTag4())){
                tags.add(output.getTag4());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(tags, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/commentSummery",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SentmentModelRespons> commentSummery(@QueryParam("nid") Long nid)
        throws URISyntaxException {
        News old = newsService.findOne(nid);
        SentmentModel output = new SentmentModel();
        SentmentModelRespons sentmentModelRespons = new SentmentModelRespons();
        List<News> newss = new ArrayList<>();
        log.debug("REST request to get similar articles");
        try {
            Client client = Client.create();
            String url = "http://localhost:54540/api/SentmentAnalysis?nids="+nid;
            WebResource webResource = client
                .resource(url);
            ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }
            output = response.getEntity(SentmentModel.class);

            TFIDFCalculator tfidfCalculator = new TFIDFCalculator();
            List<List<String>> posDocs;
            List<List<String>> negDocs;
            String possum = "";
            String negSum = "";

            if(output != null){
                for(String pos : output.getPossitives()){
                    posDocs = new ArrayList<>();
                    List<String> items = Arrays.asList(pos.split(" "));
                    posDocs.add(items);
                    Set<String> posStrings = tfidfCalculator.getSummeryWithWeight(posDocs);
                    possum = possum + OpenNTest.deTokenize(posStrings.stream().toArray(ne -> new String[ne]), DetokenizationDictionary.Operation.MOVE_LEFT);
                }

                for(String neg : output.getNegatives()){
                    negDocs = new ArrayList<>();
                    List<String> items = Arrays.asList(neg.split(" "));
                    negDocs.add(items);
                    Set<String> negStrings = tfidfCalculator.getSummeryWithWeight(negDocs);
                    negSum = negSum + OpenNTest.deTokenize(negStrings.stream().toArray(ne -> new String[ne]), DetokenizationDictionary.Operation.MOVE_LEFT);
                }
            }
            sentmentModelRespons.setNegatives(negSum);
            sentmentModelRespons.setPossitives(possum);

        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(sentmentModelRespons, headers, HttpStatus.OK);
    }

}
