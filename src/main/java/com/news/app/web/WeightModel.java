package com.news.app.web;

/**
 * Created by shashika silva on 28/05/2017.
 */
public class WeightModel {
    private String model;
    private Double weight;
    private String mWord;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getmWord() {
        return mWord;
    }

    public void setmWord(String mWord) {
        this.mWord = mWord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeightModel that = (WeightModel) o;

        if (model != null ? !model.equals(that.model) : that.model != null) return false;
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) return false;
        return mWord != null ? mWord.equals(that.mWord) : that.mWord == null;
    }

    @Override
    public int hashCode() {
        int result = model != null ? model.hashCode() : 0;
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (mWord != null ? mWord.hashCode() : 0);
        return result;
    }
}
