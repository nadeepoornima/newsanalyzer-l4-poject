(function() {
    'use strict';

    angular
        .module('newsappApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
