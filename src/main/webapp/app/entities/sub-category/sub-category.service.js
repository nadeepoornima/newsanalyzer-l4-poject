(function() {
    'use strict';
    angular
        .module('newsappApp')
        .factory('SubCategory', SubCategory);

    SubCategory.$inject = ['$resource', 'DateUtils'];

    function SubCategory ($resource, DateUtils) {
        var resourceUrl =  'api/sub-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createDate = DateUtils.convertDateTimeFromServer(data.createDate);
                        data.updateDate = DateUtils.convertDateTimeFromServer(data.updateDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
