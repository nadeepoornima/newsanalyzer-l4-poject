(function() {
    'use strict';

    angular
        .module('newsappApp')
        .controller('NewsDetailController', NewsDetailController);

    NewsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'News', 'SubCategory', 'User', 'Comment','$window'];

    function NewsDetailController($scope, $rootScope, $stateParams, previousState, entity, News, SubCategory, User, Comment ,$window) {
        var vm = this;
        vm.news = entity;
        vm.saveComment = saveComment;
        vm.updateLike = updateLike;
        vm.previousState = previousState.name;
        vm.commentsList = Comment.commentsByNewsId({id: entity.id});
        vm.similerArticles = News.getSimilerArticlesClustringService({aid: entity.createBy.id},{nid: entity.id});
        vm.connectedArticles = News.getConnectedService({nid: entity.id});
        vm.commentSummery = News.commentSummery({nid: entity.id});
        var unsubscribe = $rootScope.$on('newsappApp:newsUpdate', function(event, result) {
            vm.news = result;
        });
        $scope.$on('$destroy', unsubscribe);

        function updateLike() {
            vm.isSaving = true;
            if (entity.id !== null) {
                entity.noOfLikes = entity.noOfLikes + 1;
                News.update(entity, onSaveSuccess, onSaveError);
            }
        }


        function saveComment () {
            vm.isSaving = true;
            vm.comment.isActive = true;
            if($("#field_rating").val() > 0) {
                vm.comment.rating = $("#field_rating").val();
            }
            vm.comment.news = entity;
            if (vm.comment.id !== null) {
                Comment.update(vm.comment, onSaveSuccess, onSaveError);
            } else {
                Comment.save(vm.comment, onSaveSuccess, onSaveError);
            }
            $('#field_rating').val('');
            $('#field_userComment').val('');

        }

        function onSaveSuccess (result) {
            $scope.$emit('newsappApp:newsUpdate', result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

    }
})();
