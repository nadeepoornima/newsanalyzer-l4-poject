(function() {
    'use strict';
    angular
        .module('newsappApp')
        .factory('News', News);

    News.$inject = ['$resource', 'DateUtils'];

    function News ($resource, DateUtils) {
        var resourceUrl =  'api/news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createDate = DateUtils.convertDateTimeFromServer(data.createDate);
                        data.updateDate = DateUtils.convertDateTimeFromServer(data.updateDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'getSimilerArticlesClustringService':{
                                        url: 'api/similarArticles?aid=:aid&nid=:nid',
                                        method: 'GET',
                                        params:{
                                            aid:'@aid',
                                            nid:'@nid'
                                        },
                                        isArray: true
                                    },
            'getConnectedService':{
                                        url: 'api/connectedArticles?nid=:nid',
                                        method: 'GET',
                                        params:{
                                            nid:'@nid'
                                        },
                                        isArray: true
                                    },
            'getPublishArea':{
                                        url: 'api/publishArea',
                                        method: 'GET',
                                        params:{
                                        },
                                        isArray: true
                                    },
            'commentSummery':{
                                        url: 'api/commentSummery?nid=:nid',
                                        method: 'GET',
                                        params:{
                                            nid:'@nid'
                                        },
                                        isArray: true
                                    }
        });
    }
})();
