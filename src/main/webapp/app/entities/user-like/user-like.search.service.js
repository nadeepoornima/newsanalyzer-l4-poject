(function() {
    'use strict';

    angular
        .module('newsappApp')
        .factory('UserLikeSearch', UserLikeSearch);

    UserLikeSearch.$inject = ['$resource'];

    function UserLikeSearch($resource) {
        var resourceUrl =  'api/_search/user-likes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
