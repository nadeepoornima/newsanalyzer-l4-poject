(function() {
    'use strict';

    angular
        .module('newsappApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-like', {
            parent: 'entity',
            url: '/user-like?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserLikes'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-like/user-likes.html',
                    controller: 'UserLikeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('user-like-detail', {
            parent: 'entity',
            url: '/user-like/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'UserLike'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-like/user-like-detail.html',
                    controller: 'UserLikeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UserLike', function($stateParams, UserLike) {
                    return UserLike.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-like',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-like-detail.edit', {
            parent: 'user-like-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-like/user-like-dialog.html',
                    controller: 'UserLikeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserLike', function(UserLike) {
                            return UserLike.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-like.new', {
            parent: 'user-like',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-like/user-like-dialog.html',
                    controller: 'UserLikeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                isLike: false,
                                isActive: null,
                                createDate: null,
                                updateDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-like', null, { reload: 'user-like' });
                }, function() {
                    $state.go('user-like');
                });
            }]
        })
        .state('user-like.edit', {
            parent: 'user-like',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-like/user-like-dialog.html',
                    controller: 'UserLikeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserLike', function(UserLike) {
                            return UserLike.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-like', null, { reload: 'user-like' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-like.delete', {
            parent: 'user-like',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-like/user-like-delete-dialog.html',
                    controller: 'UserLikeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserLike', function(UserLike) {
                            return UserLike.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-like', null, { reload: 'user-like' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
