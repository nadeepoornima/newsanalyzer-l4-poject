(function() {
    'use strict';

    angular
        .module('newsappApp')
        .controller('UserLikeDialogController', UserLikeDialogController);

    UserLikeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserLike', 'News', 'User'];

    function UserLikeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserLike, News, User) {
        var vm = this;

        vm.userLike = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.news = News.query();
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userLike.id !== null) {
                UserLike.update(vm.userLike, onSaveSuccess, onSaveError);
            } else {
                UserLike.save(vm.userLike, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newsappApp:userLikeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createDate = false;
        vm.datePickerOpenStatus.updateDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
