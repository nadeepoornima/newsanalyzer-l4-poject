(function() {
    'use strict';
    angular
        .module('newsappApp')
        .factory('UserLike', UserLike);

    UserLike.$inject = ['$resource', 'DateUtils'];

    function UserLike ($resource, DateUtils) {
        var resourceUrl =  'api/user-likes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createDate = DateUtils.convertDateTimeFromServer(data.createDate);
                        data.updateDate = DateUtils.convertDateTimeFromServer(data.updateDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
