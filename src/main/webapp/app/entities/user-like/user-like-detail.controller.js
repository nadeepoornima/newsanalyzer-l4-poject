(function() {
    'use strict';

    angular
        .module('newsappApp')
        .controller('UserLikeDetailController', UserLikeDetailController);

    UserLikeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserLike', 'News', 'User'];

    function UserLikeDetailController($scope, $rootScope, $stateParams, previousState, entity, UserLike, News, User) {
        var vm = this;

        vm.userLike = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newsappApp:userLikeUpdate', function(event, result) {
            vm.userLike = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
