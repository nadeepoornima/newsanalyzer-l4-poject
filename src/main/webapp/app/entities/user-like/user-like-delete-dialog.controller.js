(function() {
    'use strict';

    angular
        .module('newsappApp')
        .controller('UserLikeDeleteController',UserLikeDeleteController);

    UserLikeDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserLike'];

    function UserLikeDeleteController($uibModalInstance, entity, UserLike) {
        var vm = this;

        vm.userLike = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserLike.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
