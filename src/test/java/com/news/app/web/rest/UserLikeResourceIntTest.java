package com.news.app.web.rest;

import com.news.app.NewsappApp;

import com.news.app.domain.UserLike;
import com.news.app.repository.UserLikeRepository;
import com.news.app.service.UserLikeService;
import com.news.app.repository.search.UserLikeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserLikeResource REST controller.
 *
 * @see UserLikeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewsappApp.class)
public class UserLikeResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final Boolean DEFAULT_IS_LIKE = false;
    private static final Boolean UPDATED_IS_LIKE = true;

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATE_DATE_STR = dateTimeFormatter.format(DEFAULT_CREATE_DATE);

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATE_DATE_STR = dateTimeFormatter.format(DEFAULT_UPDATE_DATE);

    @Inject
    private UserLikeRepository userLikeRepository;

    @Inject
    private UserLikeService userLikeService;

    @Inject
    private UserLikeSearchRepository userLikeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restUserLikeMockMvc;

    private UserLike userLike;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserLikeResource userLikeResource = new UserLikeResource();
        ReflectionTestUtils.setField(userLikeResource, "userLikeService", userLikeService);
        this.restUserLikeMockMvc = MockMvcBuilders.standaloneSetup(userLikeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLike createEntity(EntityManager em) {
        UserLike userLike = new UserLike()
                .isLike(DEFAULT_IS_LIKE)
                .isActive(DEFAULT_IS_ACTIVE)
                .createDate(DEFAULT_CREATE_DATE)
                .updateDate(DEFAULT_UPDATE_DATE);
        return userLike;
    }

    @Before
    public void initTest() {
        userLikeSearchRepository.deleteAll();
        userLike = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserLike() throws Exception {
        int databaseSizeBeforeCreate = userLikeRepository.findAll().size();

        // Create the UserLike

        restUserLikeMockMvc.perform(post("/api/user-likes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userLike)))
                .andExpect(status().isCreated());

        // Validate the UserLike in the database
        List<UserLike> userLikes = userLikeRepository.findAll();
        assertThat(userLikes).hasSize(databaseSizeBeforeCreate + 1);
        UserLike testUserLike = userLikes.get(userLikes.size() - 1);
        assertThat(testUserLike.isIsLike()).isEqualTo(DEFAULT_IS_LIKE);
        assertThat(testUserLike.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testUserLike.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testUserLike.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);

        // Validate the UserLike in ElasticSearch
        UserLike userLikeEs = userLikeSearchRepository.findOne(testUserLike.getId());
        assertThat(userLikeEs).isEqualToComparingFieldByField(testUserLike);
    }

    @Test
    @Transactional
    public void checkIsLikeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userLikeRepository.findAll().size();
        // set the field null
        userLike.setIsLike(null);

        // Create the UserLike, which fails.

        restUserLikeMockMvc.perform(post("/api/user-likes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userLike)))
                .andExpect(status().isBadRequest());

        List<UserLike> userLikes = userLikeRepository.findAll();
        assertThat(userLikes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserLikes() throws Exception {
        // Initialize the database
        userLikeRepository.saveAndFlush(userLike);

        // Get all the userLikes
        restUserLikeMockMvc.perform(get("/api/user-likes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userLike.getId().intValue())))
                .andExpect(jsonPath("$.[*].isLike").value(hasItem(DEFAULT_IS_LIKE.booleanValue())))
                .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
                .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE_STR)));
    }

    @Test
    @Transactional
    public void getUserLike() throws Exception {
        // Initialize the database
        userLikeRepository.saveAndFlush(userLike);

        // Get the userLike
        restUserLikeMockMvc.perform(get("/api/user-likes/{id}", userLike.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userLike.getId().intValue()))
            .andExpect(jsonPath("$.isLike").value(DEFAULT_IS_LIKE.booleanValue()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE_STR))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingUserLike() throws Exception {
        // Get the userLike
        restUserLikeMockMvc.perform(get("/api/user-likes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserLike() throws Exception {
        // Initialize the database
        userLikeService.save(userLike);

        int databaseSizeBeforeUpdate = userLikeRepository.findAll().size();

        // Update the userLike
        UserLike updatedUserLike = userLikeRepository.findOne(userLike.getId());
        updatedUserLike
                .isLike(UPDATED_IS_LIKE)
                .isActive(UPDATED_IS_ACTIVE)
                .createDate(UPDATED_CREATE_DATE)
                .updateDate(UPDATED_UPDATE_DATE);

        restUserLikeMockMvc.perform(put("/api/user-likes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedUserLike)))
                .andExpect(status().isOk());

        // Validate the UserLike in the database
        List<UserLike> userLikes = userLikeRepository.findAll();
        assertThat(userLikes).hasSize(databaseSizeBeforeUpdate);
        UserLike testUserLike = userLikes.get(userLikes.size() - 1);
        assertThat(testUserLike.isIsLike()).isEqualTo(UPDATED_IS_LIKE);
        assertThat(testUserLike.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testUserLike.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testUserLike.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);

        // Validate the UserLike in ElasticSearch
        UserLike userLikeEs = userLikeSearchRepository.findOne(testUserLike.getId());
        assertThat(userLikeEs).isEqualToComparingFieldByField(testUserLike);
    }

    @Test
    @Transactional
    public void deleteUserLike() throws Exception {
        // Initialize the database
        userLikeService.save(userLike);

        int databaseSizeBeforeDelete = userLikeRepository.findAll().size();

        // Get the userLike
        restUserLikeMockMvc.perform(delete("/api/user-likes/{id}", userLike.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean userLikeExistsInEs = userLikeSearchRepository.exists(userLike.getId());
        assertThat(userLikeExistsInEs).isFalse();

        // Validate the database is empty
        List<UserLike> userLikes = userLikeRepository.findAll();
        assertThat(userLikes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUserLike() throws Exception {
        // Initialize the database
        userLikeService.save(userLike);

        // Search the userLike
        restUserLikeMockMvc.perform(get("/api/_search/user-likes?query=id:" + userLike.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLike.getId().intValue())))
            .andExpect(jsonPath("$.[*].isLike").value(hasItem(DEFAULT_IS_LIKE.booleanValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE_STR)));
    }
}
