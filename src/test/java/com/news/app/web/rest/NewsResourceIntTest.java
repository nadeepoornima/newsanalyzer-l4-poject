package com.news.app.web.rest;

import com.news.app.NewsappApp;

import com.news.app.domain.News;
import com.news.app.repository.NewsRepository;
import com.news.app.service.NewsService;
import com.news.app.repository.search.NewsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsResource REST controller.
 *
 * @see NewsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewsappApp.class)
public class NewsResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NEWS_TITLE = "AAAAA";
    private static final String UPDATED_NEWS_TITLE = "BBBBB";
    private static final String DEFAULT_NEWS_DESCRIPTION = "AAAAA";
    private static final String UPDATED_NEWS_DESCRIPTION = "BBBBB";

    private static final Integer DEFAULT_NO_OF_LIKES = 1;
    private static final Integer UPDATED_NO_OF_LIKES = 2;

    private static final Integer DEFAULT_NO_OF_SHARES = 1;
    private static final Integer UPDATED_NO_OF_SHARES = 2;

    private static final Integer DEFAULT_NO_OF_VIEWS = 1;
    private static final Integer UPDATED_NO_OF_VIEWS = 2;
    private static final String DEFAULT_EMOTIONS = "AAAAA";
    private static final String UPDATED_EMOTIONS = "BBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATE_DATE_STR = dateTimeFormatter.format(DEFAULT_CREATE_DATE);

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATE_DATE_STR = dateTimeFormatter.format(DEFAULT_UPDATE_DATE);

    @Inject
    private NewsRepository newsRepository;

    @Inject
    private NewsService newsService;

    @Inject
    private NewsSearchRepository newsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restNewsMockMvc;

    private News news;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NewsResource newsResource = new NewsResource();
        ReflectionTestUtils.setField(newsResource, "newsService", newsService);
        this.restNewsMockMvc = MockMvcBuilders.standaloneSetup(newsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static News createEntity(EntityManager em) {
        News news = new News()
                .newsTitle(DEFAULT_NEWS_TITLE)
                .newsDescription(DEFAULT_NEWS_DESCRIPTION)
                .noOfLikes(DEFAULT_NO_OF_LIKES)
                .noOfShares(DEFAULT_NO_OF_SHARES)
                .noOfViews(DEFAULT_NO_OF_VIEWS)
                .emotions(DEFAULT_EMOTIONS)
                .isActive(DEFAULT_IS_ACTIVE)
                .createDate(DEFAULT_CREATE_DATE)
                .updateDate(DEFAULT_UPDATE_DATE);
        return news;
    }

    @Before
    public void initTest() {
        newsSearchRepository.deleteAll();
        news = createEntity(em);
    }

    @Test
    @Transactional
    public void createNews() throws Exception {
        int databaseSizeBeforeCreate = newsRepository.findAll().size();

        // Create the News

        restNewsMockMvc.perform(post("/api/news")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(news)))
                .andExpect(status().isCreated());

        // Validate the News in the database
        List<News> news = newsRepository.findAll();
        assertThat(news).hasSize(databaseSizeBeforeCreate + 1);
        News testNews = news.get(news.size() - 1);
        assertThat(testNews.getNewsTitle()).isEqualTo(DEFAULT_NEWS_TITLE);
        assertThat(testNews.getNewsDescription()).isEqualTo(DEFAULT_NEWS_DESCRIPTION);
        assertThat(testNews.getNoOfLikes()).isEqualTo(DEFAULT_NO_OF_LIKES);
        assertThat(testNews.getNoOfShares()).isEqualTo(DEFAULT_NO_OF_SHARES);
        assertThat(testNews.getNoOfViews()).isEqualTo(DEFAULT_NO_OF_VIEWS);
        assertThat(testNews.getEmotions()).isEqualTo(DEFAULT_EMOTIONS);
        assertThat(testNews.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testNews.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testNews.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);

        // Validate the News in ElasticSearch
        News newsEs = newsSearchRepository.findOne(testNews.getId());
        assertThat(newsEs).isEqualToComparingFieldByField(testNews);
    }

    @Test
    @Transactional
    public void checkNewsTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsRepository.findAll().size();
        // set the field null
        news.setNewsTitle(null);

        // Create the News, which fails.

        restNewsMockMvc.perform(post("/api/news")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(news)))
                .andExpect(status().isBadRequest());

        List<News> news = newsRepository.findAll();
        assertThat(news).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNewsDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsRepository.findAll().size();
        // set the field null
        news.setNewsDescription(null);

        // Create the News, which fails.

        restNewsMockMvc.perform(post("/api/news")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(news)))
                .andExpect(status().isBadRequest());

        List<News> news = newsRepository.findAll();
        assertThat(news).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNews() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the news
        restNewsMockMvc.perform(get("/api/news?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(news.getId().intValue())))
                .andExpect(jsonPath("$.[*].newsTitle").value(hasItem(DEFAULT_NEWS_TITLE.toString())))
                .andExpect(jsonPath("$.[*].newsDescription").value(hasItem(DEFAULT_NEWS_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].noOfLikes").value(hasItem(DEFAULT_NO_OF_LIKES)))
                .andExpect(jsonPath("$.[*].noOfShares").value(hasItem(DEFAULT_NO_OF_SHARES)))
                .andExpect(jsonPath("$.[*].noOfViews").value(hasItem(DEFAULT_NO_OF_VIEWS)))
                .andExpect(jsonPath("$.[*].emotions").value(hasItem(DEFAULT_EMOTIONS.toString())))
                .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
                .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE_STR)));
    }

    @Test
    @Transactional
    public void getNews() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get the news
        restNewsMockMvc.perform(get("/api/news/{id}", news.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(news.getId().intValue()))
            .andExpect(jsonPath("$.newsTitle").value(DEFAULT_NEWS_TITLE.toString()))
            .andExpect(jsonPath("$.newsDescription").value(DEFAULT_NEWS_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.noOfLikes").value(DEFAULT_NO_OF_LIKES))
            .andExpect(jsonPath("$.noOfShares").value(DEFAULT_NO_OF_SHARES))
            .andExpect(jsonPath("$.noOfViews").value(DEFAULT_NO_OF_VIEWS))
            .andExpect(jsonPath("$.emotions").value(DEFAULT_EMOTIONS.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE_STR))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingNews() throws Exception {
        // Get the news
        restNewsMockMvc.perform(get("/api/news/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        int databaseSizeBeforeUpdate = newsRepository.findAll().size();

        // Update the news
        News updatedNews = newsRepository.findOne(news.getId());
        updatedNews
                .newsTitle(UPDATED_NEWS_TITLE)
                .newsDescription(UPDATED_NEWS_DESCRIPTION)
                .noOfLikes(UPDATED_NO_OF_LIKES)
                .noOfShares(UPDATED_NO_OF_SHARES)
                .noOfViews(UPDATED_NO_OF_VIEWS)
                .emotions(UPDATED_EMOTIONS)
                .isActive(UPDATED_IS_ACTIVE)
                .createDate(UPDATED_CREATE_DATE)
                .updateDate(UPDATED_UPDATE_DATE);

        restNewsMockMvc.perform(put("/api/news")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNews)))
                .andExpect(status().isOk());

        // Validate the News in the database
        List<News> news = newsRepository.findAll();
        assertThat(news).hasSize(databaseSizeBeforeUpdate);
        News testNews = news.get(news.size() - 1);
        assertThat(testNews.getNewsTitle()).isEqualTo(UPDATED_NEWS_TITLE);
        assertThat(testNews.getNewsDescription()).isEqualTo(UPDATED_NEWS_DESCRIPTION);
        assertThat(testNews.getNoOfLikes()).isEqualTo(UPDATED_NO_OF_LIKES);
        assertThat(testNews.getNoOfShares()).isEqualTo(UPDATED_NO_OF_SHARES);
        assertThat(testNews.getNoOfViews()).isEqualTo(UPDATED_NO_OF_VIEWS);
        assertThat(testNews.getEmotions()).isEqualTo(UPDATED_EMOTIONS);
        assertThat(testNews.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testNews.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testNews.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);

        // Validate the News in ElasticSearch
        News newsEs = newsSearchRepository.findOne(testNews.getId());
        assertThat(newsEs).isEqualToComparingFieldByField(testNews);
    }

    @Test
    @Transactional
    public void deleteNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        int databaseSizeBeforeDelete = newsRepository.findAll().size();

        // Get the news
        restNewsMockMvc.perform(delete("/api/news/{id}", news.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean newsExistsInEs = newsSearchRepository.exists(news.getId());
        assertThat(newsExistsInEs).isFalse();

        // Validate the database is empty
        List<News> news = newsRepository.findAll();
        assertThat(news).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        // Search the news
        restNewsMockMvc.perform(get("/api/_search/news?query=id:" + news.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(news.getId().intValue())))
            .andExpect(jsonPath("$.[*].newsTitle").value(hasItem(DEFAULT_NEWS_TITLE.toString())))
            .andExpect(jsonPath("$.[*].newsDescription").value(hasItem(DEFAULT_NEWS_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].noOfLikes").value(hasItem(DEFAULT_NO_OF_LIKES)))
            .andExpect(jsonPath("$.[*].noOfShares").value(hasItem(DEFAULT_NO_OF_SHARES)))
            .andExpect(jsonPath("$.[*].noOfViews").value(hasItem(DEFAULT_NO_OF_VIEWS)))
            .andExpect(jsonPath("$.[*].emotions").value(hasItem(DEFAULT_EMOTIONS.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE_STR)));
    }
}
